import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { Person } from "../../models/person";
import { PersonService } from "../../services/person.service";
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from "@angular/material/table";
import { MatSort } from "@angular/material/sort";
import { Subject, takeUntil } from "rxjs";

@Component({
	selector: "app-person-list",
	templateUrl: "./person-list.component.html",
	styleUrls: ["./person-list.component.scss"],
	providers: [PersonService]
})
export class PersonListComponent implements OnInit, AfterViewInit, OnDestroy {

	@ViewChild(MatPaginator) paginator!: MatPaginator;
	@ViewChild(MatSort) sort!: MatSort;


	displayedColumns: string[] = ["id", "firstName", "lastName", "gender", "email"];
	dataSource = new MatTableDataSource<Person>();

	destroy$ = new Subject<void>();

	constructor(private personSrv: PersonService) {
	}

	/**
	 * Initialisation du composant
	 * Souscription à l'observable contenant les données générées pour affichage dans le tableau
	 */
	ngOnInit(): void {
		this.personSrv.generatedPersons$.pipe(takeUntil(this.destroy$)).subscribe(value => this.dataSource.data = value);
	}

	/**
	 * Initialise les modules de tri et de pagination
	 */
	ngAfterViewInit(): void {
	  this.dataSource.paginator = this.paginator;
	  this.dataSource.sort = this.sort;
	}

	ngOnDestroy(): void {
		this.destroy$.next();
	}
}
