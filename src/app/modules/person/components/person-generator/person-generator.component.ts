import { Component, OnInit } from "@angular/core";
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from "@angular/forms";
import { GenerationConfigInterface } from "../../models/generation-config";
import { PersonService } from "../../services/person.service";

@Component({
	selector: "app-person-generator",
	templateUrl: "./person-generator.component.html",
	styleUrls: ["./person-generator.component.scss"]
})
export class PersonGeneratorComponent implements OnInit {

	generatorForm!: FormGroup;

	constructor(private formBuilder: FormBuilder, private readonly personSrv: PersonService) {
	}

	/**
	 * Initialisation du composant et du formulaire
	 */
	ngOnInit() {
		this.generatorForm = this.formBuilder.group({
			count: [1000, [Validators.min(0), Validators.max(1000)]],
			male: [true],
			female: [true]
		}, { validators: [this.genderValidator()] }
		);
	}

	/**
	 * Demande la génération des données selon la config passée
	 */
	generate() {
		const value: GenerationConfigInterface = this.generatorForm.value;
		if (this.generatorForm.valid) this.personSrv.getPersons(value);
	}

	/**
	 * Custom validator qui test qu'on a sélectionné au moins 1 genre
	 * @returns ValidatorFn
	 */
	genderValidator(): ValidatorFn {
		return (control: AbstractControl): ValidationErrors | null => {
			if (!control.get('male')?.value && !control.get('female')?.value) return { genderError: true };
			return null;
		}
	}

}
