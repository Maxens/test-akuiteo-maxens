import { Pipe, PipeTransform } from '@angular/core';

/**
 * Pipe qui change l'affichage du genre
 */
@Pipe({
  name: 'genderPromptPipe'
})
export class GenderPromptPipe implements PipeTransform {

  transform(value: string): string {
    return value === 'Male' ? 'Homme' : 'Femme';
  }

}
