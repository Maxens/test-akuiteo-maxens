import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { NoopAnimationsModule } from "@angular/platform-browser/animations";
import { Spectator } from "@ngneat/spectator";
import { createComponentFactory } from "@ngneat/spectator/jest";
import { PersonGeneratorComponent } from "./person-generator.component";
import { PersonService } from "../../services/person.service";
import { HttpClientTestingModule } from "@angular/common/http/testing";
import { MatIconModule } from "@angular/material/icon";

describe("PersonGeneratorComponent", () => {

	let spectator: Spectator<PersonGeneratorComponent>;
	const createComponent = createComponentFactory({
		component: PersonGeneratorComponent,
		declarations: [
			PersonGeneratorComponent,
		],
		imports: [
			MatCheckboxModule,
			MatFormFieldModule,
			MatInputModule,
			MatButtonModule,
			ReactiveFormsModule,
			NoopAnimationsModule,
			HttpClientTestingModule,
			MatIconModule
		],
		providers: [PersonService]
	});

	beforeEach(() => {
		spectator = createComponent();
	});


	test('should create', () => {
		expect(spectator.component).toBeTruthy();
	});

	test('generate', () => {
		const personSrv = spectator.inject(PersonService, true);
		const srvSpy = jest.spyOn(personSrv, 'getPersons');
		spectator.component.generate();
		expect(srvSpy).toHaveBeenCalledTimes(1);

		spectator.component.generatorForm.controls['male'].setValue(false);
		spectator.component.generatorForm.controls['female'].setValue(false);
		spectator.component.generate();
		expect(srvSpy).toHaveBeenCalledTimes(1);

		spectator.component.generatorForm.controls['male'].setValue(true);
		spectator.component.generatorForm.controls['female'].setValue(true);
		spectator.component.generatorForm.controls['count'].setValue(-1);
		spectator.component.generate();
		expect(srvSpy).toHaveBeenCalledTimes(1);

		spectator.component.generatorForm.controls['count'].setValue(1001);
		spectator.component.generate();
		expect(srvSpy).toHaveBeenCalledTimes(1);
	});
});

