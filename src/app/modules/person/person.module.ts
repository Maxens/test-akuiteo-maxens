import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatTableModule } from "@angular/material/table";
import { PersonGeneratorComponent } from "./components/person-generator/person-generator.component";
import { PersonListComponent } from "./components/person-list/person-list.component";
import { PersonRoutingModule } from "./person-routing.module";
import { PersonService } from "./services/person.service";
import { GenderPromptPipe } from './pipe/gender-prompt.pipe';
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSortModule } from "@angular/material/sort";
import { MatIconModule } from "@angular/material/icon";

@NgModule({
	declarations: [
		PersonListComponent,
		PersonGeneratorComponent,
  		GenderPromptPipe
	],
	imports: [
		CommonModule,
		PersonRoutingModule,
		MatFormFieldModule,
		MatInputModule,
		MatButtonModule,
		MatCheckboxModule,
		MatTableModule,
		ReactiveFormsModule,
		MatPaginatorModule,
		MatSortModule,
		MatIconModule
	],
	providers: [PersonService]
})
export class PersonModule {}
