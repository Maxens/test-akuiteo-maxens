
export interface GenerationConfigInterface {
	count: number;
	male: boolean;
	female: boolean;
}
