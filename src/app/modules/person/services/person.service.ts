import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { GenerationConfigInterface } from "../models/generation-config";
import { Person } from "../models/person";

@Injectable()
export class PersonService {

	generatedPersons$ = new BehaviorSubject<Array<Person>>([]);

	constructor(private http: HttpClient) {
	}

	/**
	 * Récupère les données puis les filtre selon les critères passés en paramètres
	 * puis les met à dispo dans un observable
	 * @param config 
	 */
	getPersons(config: GenerationConfigInterface): void {
		if (config.count > 0) {
			this.http.get<Person[]>("/assets/data/persons.json").subscribe(value => {
				const results: Array<Person> = [];
	
				for (const person of value) {
					if ((person.gender === 'Male' && config.male) || 
						((person.gender === 'Female' && config.female))){
							results.push(person);
						} 
					if (results.length === config.count) break;
				}
	
				this.generatedPersons$.next(results);
			});
		}
	}
}
