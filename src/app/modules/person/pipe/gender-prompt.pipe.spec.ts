import { GenderPromptPipe } from './gender-prompt.pipe';

describe('SexePromptPipe', () => {

  const pipe = new GenderPromptPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('transform', () => {
    const test1 = pipe.transform('Male');
    const test2 = pipe.transform('Female');

    expect(test1).toEqual('Homme');
    expect(test2).toEqual('Femme');
  });
});
