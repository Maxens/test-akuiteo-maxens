import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { MatButtonModule } from "@angular/material/button";
import { AboutRoutingModule } from "./about-routing.module";
import { AboutComponent } from "./components/about.component";
import { MatIconModule } from '@angular/material/icon';

@NgModule({
	declarations: [AboutComponent],
	imports: [
		CommonModule,
		MatButtonModule,
		AboutRoutingModule,
		MatIconModule
	]
})
export class AboutModule {
}
