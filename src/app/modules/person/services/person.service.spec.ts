import { HttpMethod, SpectatorHttp } from "@ngneat/spectator";

import { createHttpFactory } from '@ngneat/spectator/jest';
import { Person } from "../models/person";
import { PersonService } from "./person.service";


const PERSONS: Person[] = [
	{
		id: 1,
		firstName: "John",
		lastName: "REESE",
		email: "john@reese.com",
		gender: "Male"
	},
	{
		id: 2,
		firstName: "Harold",
		lastName: "FINCH",
		email: "harold@finch.com",
		gender: "Male"
	},
	{
		id: 3,
		firstName: "Joss",
		lastName: "CARTER",
		email: "joss@carter.com",
		gender: "Female"
	}
];

const DEFAULT_CONFIG = {
	count: 3,
	male: true,
	female: true
};

describe("PersonService", () => {

	let spectator: SpectatorHttp<PersonService>;

	const createHttp = createHttpFactory(PersonService);


	beforeEach(() => spectator = createHttp());

	test('should create', () => {
		expect(spectator.service).toBeTruthy();
	});

	test("should provide a list of 3 persons", () => {

		const reqUrl = '/assets/data/persons.json';

		expect(spectator.service.getPersons).toBeTruthy();

		spectator.service.getPersons(DEFAULT_CONFIG);
		const req = spectator.expectOne(reqUrl, HttpMethod.GET);
		spectator.service.generatedPersons$.subscribe(value => {
			expect(value.length).toEqual(3);
		});

		/*const reqs = spectator.expectConcurrent([
			{ url: reqUrl, method: HttpMethod.GET },
		]);*/

		spectator.flushAll([req], [PERSONS]);
	});


});

